FROM alpine as builder

COPY configure.sh /usr/local/bin/configure
COPY daemon.sh /usr/bin/my-daemon

LABEL maintainer="Jay MOULIN <https://jaymoulin.me/me/docker-transmission>"
ARG VERSION=4.0.6
ARG TARGETPLATFORM
LABEL version=${VERSION}-${TARGETPLATFORM}

RUN echo "http://dl-4.alpinelinux.org/alpine/latest-stable/main/" >> /etc/apk/repositories && \
apk update && apk upgrade && \
apk add transmission-daemon wget curl --no-cache && \
wget "https://raw.githubusercontent.com/ronggang/transmission-web-control/master/release/install-tr-control.sh" -O /tmp/install.sh && \
(echo 1 | sh /tmp/install.sh) && \
rm /tmp/install.sh && \
mkdir /output && \
mkdir /to_download && \
mkdir /config &&\
chmod 777 /output && \
chmod 777 /to_download && \
chmod 777 /config && \
apk del wget curl --purge
WORKDIR /usr/bin

EXPOSE 9091
EXPOSE 51413
EXPOSE 51413/udp

VOLUME /output
VOLUME /to_download
VOLUME /config

ENV USERNAME=admin
ENV PASSWORD=admin
ENV PORT=9091

CMD my-daemon
