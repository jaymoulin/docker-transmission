VERSION ?= 4.0.6
CACHE ?= --no-cache=1

.PHONY: all build publish
all: build publish
build:
	docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64,linux/386,linux/arm/v6 ${PUSH} --build-arg VERSION=${VERSION} --tag jaymoulin/transmission --tag jaymoulin/transmission:${VERSION} ${CACHE} .
publish:
	PUSH=--push CACHE= make build

